package pl.edu.pwsztar;

public class Account {
    private double accountNumber = 0;
    private double balance = 0;
    public Account(double accNo, double balance, String cName, String email, String pNo){
        this.accountNumber = accNo;
        this.balance = balance;
    }

    public double getBalance(){
        return balance;
    }

    public double getAccountNumber(){
        return accountNumber;
    }

    public double depositFund(double amount){
        if (amount>0) {
            this.balance += amount;
            return balance;
        }
        System.out.println("Error!");
        return -1;
    }

    public double withdrawFund(double amount){
        if(amount>0 && (this.balance-amount)>0){
            this.balance -= amount;
            return balance;
        }
        System.out.println("Error!");
        return -1;
    }
}